# cdl1 - Sensor based Activity Recognition

In the challenge cdl1 sensor based activity recognition, machine learning models should be used to classify the individual activities.

The framework conditions for the challenge were defined across the groups before the start of the challenge. This included which and how activities are recorded, where they are stored and how they are stored. 

The ReadME file will contain the following items:

- Framework conditions
- Description of the group procedure
- Information about the repository 

## Framework conditions

It was defined that 6 activities are recorded:

- Treppenlaufen
- Velofahren
- Gehen
- Rennen
- Sitzen
- Stehen

These activities were recorded using the "Sensor Logger" app. All 10 participants of the challenge had to record at least 10 minutes per activity. The minimum time of a recording was set at 1 minute and the maximum time of a recording was unlimited. 

A detailed description of the framework conditions can be found in the 1_data_understanding file.

## Description of the group procedure

The procedure was continuous. First, the recorded data were examined and understood. This procedure can be found in file 1_data_understanding. Then the data was prepared for the models. This procedure can be found in file 2_data_preprocessing. After the data was examined, understood and prepared, models were trained and evaluated. The deep learning models and their procedure can be found in file 3_deep_learning_models. The non deep learning models can be found in file 4_non_deep_learning_models. After running and evaluating each model, a comparison was made between the best Deep Learning model and the best non-Deep Learning model. This comparison, its evaluation and an outlook can be found in file 5_summary.

## Information about the repository

All mentioned and submission relevant Jupyter notebooks can be found in the Abgabe folder. The data groups which have been created and which are relevant for the execution of the files are in the folder Daten. The raw data of the participants' measurements can also be found in the Daten folder. Only the raw data which are longer than 1 minute are uploaded. Additionally relevant for the submission of the Challenge is the folder Planung_und_Konzeption which contains the planning, the concept and the schedule that have already been submitted on 16.03.2023.
Archiv folder contains the work files which are not included in the final submisson. 

In order to execute the notebooks, the corresponding libraries must be read into the respective notebook. If a library is not available on the local computer, it can be installed with a -pip install *library name*.

To see the results of JupyterNotebook 3_deep_learning_models created by the deep learning models you do not need to run every cell. You can only run the cells that contain %wandb where the reports are stored and you can see the results. It can take quite some time to load the reports.

If you want to run the JupyterNotebook 3_deep_learning_models a library 'wandb' is read in. To run this notebook you have to create an account. An introduction can be found [here](https://docs.wandb.ai/quickstart). After creating an account you get an API key on your profile at wandb which you have to copy and enter in the pop up field when reading the wandb.login() and the api. In addition to that you have to create 4 single projects in your profile named 'dl-kernel-stride-pad-pool', 'dl-deep-learning', 'dl-1dcnn-model-type' and 'dl-1dcnn-windows-para'. After that, in JupyterNotebook 3_deep_learning_models you have to replace all entity= "weiping-zhang" with entity=your username. Running the JupyterNotebook can take a long time. 


In order to follow all the steps in the JupyterNotebooks, you should work through the following sequence:

- ReadME
- 1_data_undersatnding
- 2_data_preprocessing
- 3_deep_learning_models / 4_non_deep_learning_models
- 5_summary


### Groups members

Weiping Zhang and Ognjen Colovic